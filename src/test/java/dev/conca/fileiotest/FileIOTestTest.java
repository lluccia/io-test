package dev.conca.fileiotest;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.junit.Before;
import org.junit.ComparisonFailure;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import com.google.common.io.Resources;

import dev.conca.fileiotest.exception.ProcessExecutionException;
import dev.conca.fileiotest.exception.ProcessUndefinedException;

public class FileIOTestTest {

    private FileIOTest fileIOTest;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();
    
    @Before
    public void setup() {
        fileIOTest = new FileIOTest();
    }

    @Test
    public void processUnderTestMustBeDefined() {
        expectedException.expect(ProcessUndefinedException.class);
        expectedException.expectMessage("Process under test must be defined");

        fileIOTest.execute();
    }

    @Test
    public void canAssertConsoleOutput() {
        expectedException.expect(ComparisonFailure.class);
        expectedException.expectMessage("expected:<he[y] world> but was:<he[llo] world>");

        fileIOTest.setProcess("echo", "hello world");

        fileIOTest.execute();
        
        fileIOTest.assertConsoleOutputIs("hey world");
    }
    
    @Test
    public void canAssertConsoleOutputContains() {
        fileIOTest.setProcess("echo", "hello world");
        
        fileIOTest.execute();
        
        fileIOTest.assertConsoleOutputContains("hello");
    }
    
    @Test
    public void canAssertConsoleOutputMatches() {
        fileIOTest.setProcess("date", "+%Y-%m-%d %H:%M:%S");

        fileIOTest.execute();
        
        fileIOTest.assertConsoleOutputMatches("\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}\\n");
    }
    
    @Test
    public void canDefineEnvironmentVariables() {
        fileIOTest.putEnvironment("HELLO", "hello world");
        fileIOTest.setProcess("bash", "-c", "printenv | grep HELLO=");
        
        fileIOTest.execute();
        
        fileIOTest.assertConsoleOutputIs("HELLO=hello world");
    }
    
    @Test
    public void canCopyInputFileFromReference() throws IOException {
        String referenceInputResource = "input/inputfile1.txt";
        
        File inputFileDirPath = tempFolder.newFolder("inputfilepath");
        
        File inputFile = new File(inputFileDirPath, "inputfile1.txt");
        fileIOTest.copyInputFileFromResource(referenceInputResource, inputFile);

        URL resourceURL = Resources.getResource(referenceInputResource);
        FileComparator fileComparator = new FileComparator();
        fileComparator.assertEquals(resourceURL, inputFile.toURI().toURL());
    }
    
    @Test
    public void canAssertOutputfileFromReference() throws IOException {
        String referenceOutputResource = "output/expectedOutput1.txt";
        
        File outputFileDirPath = tempFolder.newFolder("outputfilepath");
        
        File outputFile = new File(outputFileDirPath, "actualOutput1.txt");
        
        fileIOTest.setProcess("sh", "-c", "echo -n hello output > " + outputFile.getAbsolutePath());
        
        fileIOTest.execute();
        
        fileIOTest.assertFileIsEqualToResource(outputFile, referenceOutputResource);
    }
    
    @Test
    public void canAssertMultipleOutputfilesFromReferences() throws IOException {
        String referenceOutputResource1 = "output/expectedOutput1.txt";
        String referenceOutputResource2 = "output/expectedOutput2.txt";
        
        File outputFileDirPath = tempFolder.newFolder("outputfilepath");
        
        File outputFile1 = new File(outputFileDirPath, "actualOutput1.txt");
        File outputFile2 = new File(outputFileDirPath, "actualOutput2.txt");
        
        fileIOTest.setProcess("sh", "-c", "echo -n hello output > " + outputFile1.getAbsolutePath() + ";" +
                                          "echo -n hello output 2 > " + outputFile2.getAbsolutePath());
        
        fileIOTest.execute();
        
        fileIOTest.assertFileIsEqualToResource(outputFile1, referenceOutputResource1);
        fileIOTest.assertFileIsEqualToResource(outputFile2, referenceOutputResource2);
    }
    
    @Test
    public void canAssertOutputfilePerLineFromReference() throws IOException {
        String referenceOutputResource = "output/expectedOutput1.txt";
        
        File outputFileDirPath = tempFolder.newFolder("outputfilepath");
        
        File outputFile = new File(outputFileDirPath, "actualOutput1.txt");
        
        fileIOTest.setProcess("sh", "-c", "echo -n hello output > " + outputFile.getAbsolutePath());
        
        fileIOTest.execute();
        
        fileIOTest.assertFileIsEqualToResourcePerLine(outputFile, referenceOutputResource);
    }
    
    @Test
    public void mustThrowExceptionIfProcessUnderTestFails() {
        expectedException.expect(ProcessExecutionException.class);

        fileIOTest.setProcess("inexistentProcess");

        fileIOTest.execute();
    }

}
