package dev.conca.fileiotest;


import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

import org.junit.Before;
import org.junit.ComparisonFailure;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

public class FileComparatorTest {

    private FileComparator fileComparator;
    
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    
    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    
    @Before
    public void setup() {
        this.fileComparator = new FileComparator(StandardCharsets.UTF_8);
    }
    
    @Test
    public void mustThrowExceptionIfExpectedFileDoesNotExistsInClasspath() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("resource input/inexistentfile1.txt not found");

        fileComparator.assertEquals("input/inexistentfile1.txt", "input/inputfile2.txt");
    }
    
    @Test
    public void mustThrowExceptionIfActualFileDoesNotExistsInClasspath() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("resource input/inexistentfile2.txt not found");
        
        fileComparator.assertEquals("input/inputfile1.txt", "input/inexistentfile2.txt");
    }
    
    @Test
    public void canCompareFilesFromClasspath() {
        expectedException.expect(ComparisonFailure.class);
        expectedException.expectMessage("expected:<firstline" + FileIOTest.LINE_SEPARATOR
                                        + "secondline[" + FileIOTest.LINE_SEPARATOR 
                                        + "thirdline]> but was:<firstline" + FileIOTest.LINE_SEPARATOR
                                        + "secondline[]>");

        fileComparator.assertEquals("input/inputfile1.txt", "input/inputfile2.txt");
    }
    
    @Test
    public void canCompareFilesFromFilesystem() throws IOException {
        File expectedFile = tempFolder.newFile("expectedFile.txt");
        File actualFile = tempFolder.newFile("actualFile.txt");

        try(PrintWriter out = new PrintWriter(expectedFile)) {
            out.print( "expectedFileContents" );
        }
        try(PrintWriter out = new PrintWriter(actualFile)) {
            out.print( "actualFileContents" );
        }

        expectedException.expect(ComparisonFailure.class);
        expectedException.expectMessage("expected:<[expected]FileContents> but was:<[actual]FileContents>");
        
        fileComparator.assertEquals(expectedFile, actualFile);
    }
    
    @Test
    public void canCompareLineByLine() throws IOException {
        File expectedFile = tempFolder.newFile("expectedFile.txt");
        File actualFile = tempFolder.newFile("actualFile.txt");

        try(PrintWriter out = new PrintWriter(expectedFile)) {
            out.println( "ABC" );
            out.println( "DEF" );
            out.println( "GHI" );
        }
        try(PrintWriter out = new PrintWriter(actualFile)) {
            out.println( "ABC" );
            out.println( "DXF" );
            out.println( "GHI" );
        }

        expectedException.expect(AssertionError.class);
        expectedException.expectMessage("line 2: expected:<D[E]F> but was:<D[X]F>");
        
        fileComparator.assertEqualsPerLine(expectedFile, actualFile);
    }
    
    @Test
    public void canCompareLineByLineWithExtraOnExpected() throws IOException {
        File expectedFile = tempFolder.newFile("expectedFile.txt");
        File actualFile = tempFolder.newFile("actualFile.txt");
        
        try(PrintWriter out = new PrintWriter(expectedFile)) {
            out.println( "ABC" );
            out.println( "DEF" );
            out.println( "GHI" );
            out.println( "JKL" );
        }
        try(PrintWriter out = new PrintWriter(actualFile)) {
            out.println( "ABC" );
            out.println( "DEF" );
            out.println( "GHI" );
        }
        
        expectedException.expect(AssertionError.class);
        expectedException.expectMessage("line 4: expected:<JKL> but was none");
        
        fileComparator.assertEqualsPerLine(expectedFile, actualFile);
    }
    
    @Test
    public void canCompareLineByLineWithExtraOnActual() throws IOException {
        File expectedFile = tempFolder.newFile("expectedFile.txt");
        File actualFile = tempFolder.newFile("actualFile.txt");
        
        try(PrintWriter out = new PrintWriter(expectedFile)) {
            out.println( "ABC" );
            out.println( "DEF" );
            out.println( "GHI" );
        }
        try(PrintWriter out = new PrintWriter(actualFile)) {
            out.println( "ABC" );
            out.println( "DEF" );
            out.println( "GHI" );
            out.println( "JKL" );
        }
        
        expectedException.expect(AssertionError.class);
        expectedException.expectMessage("line 4: expected none but was:<JKL>");
        
        fileComparator.assertEqualsPerLine(expectedFile, actualFile);
    }
    
    @Test
    public void canCompareLineByLineWithDiffAndExtraLine() throws IOException {
        File expectedFile = tempFolder.newFile("expectedFile.txt");
        File actualFile = tempFolder.newFile("actualFile.txt");
        
        try(PrintWriter out = new PrintWriter(expectedFile)) {
            out.println( "ABC" );
            out.println( "DEF" );
            out.println( "GHI" );
        }
        try(PrintWriter out = new PrintWriter(actualFile)) {
            out.println( "ABC" );
            out.println( "DXF" );
            out.println( "GHI" );
            out.println( "JKL" );
        }
        
        expectedException.expect(AssertionError.class);
        expectedException.expectMessage("line 2: expected:<D[E]F> but was:<D[X]F>" + FileIOTest.LINE_SEPARATOR +
                                        "line 4: expected none but was:<JKL>");
        
        fileComparator.assertEqualsPerLine(expectedFile, actualFile);
    }
    
    @Test
    public void canCompareFilesFromClasspathLineByLine() {
        expectedException.expect(AssertionError.class);
        expectedException.expectMessage("line 3: expected:<thirdline> but was none");

        fileComparator.assertEqualsPerLine("input/inputfile1.txt", "input/inputfile2.txt");
    }
}
