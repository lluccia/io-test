package dev.conca.fileiotest.matchers;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class RegexMatcher extends TypeSafeMatcher<String> {

    public static Matcher<String> matchesRegex(String regex) {
        return new RegexMatcher(regex);
    }
    
    private String regex;

    private RegexMatcher(String regex) {
        this.regex = regex;
    }
    
    @Override
    public void describeTo(Description description) {
        description.appendText("a string matching regex ")
                   .appendValue(regex);
    }

    @Override
    protected boolean matchesSafely(String item) {
        return item.matches(regex);
    }        
};