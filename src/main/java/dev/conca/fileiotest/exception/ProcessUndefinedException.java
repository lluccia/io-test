package dev.conca.fileiotest.exception;

public class ProcessUndefinedException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    
    public ProcessUndefinedException(String message) {
        super(message);
    }

}
