package dev.conca.fileiotest.exception;

public class ProcessExecutionException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ProcessExecutionException(Throwable cause) {
        super(cause);
    }

}
