package dev.conca.fileiotest;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.ComparisonFailure;

import com.google.common.io.Resources;

public class FileComparator {

    private Charset charset;

    public FileComparator() {
        this(StandardCharsets.UTF_8);
    }
    
    public FileComparator(Charset charset) {
        this.charset = charset;
    }
    
    public void assertEquals(String expectedResourceName, String actualResourceName) {
        URL expectedResourceURL = Resources.getResource(expectedResourceName);
        URL actualResourceURL = Resources.getResource(actualResourceName);
        
        assertEquals(expectedResourceURL, actualResourceURL);
    }

    public void assertEquals(File expectedFile, File actualFile) {
        try {
            assertEquals(expectedFile.toURI().toURL(), actualFile.toURI().toURL());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    public void assertEquals(URL expectedURL, URL actualURL) {
        try {
            String expectedText = Resources.toString(expectedURL, charset);
            String actualText = Resources.toString(actualURL, charset);
            
            Assert.assertEquals(expectedText, actualText);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    public void assertEqualsPerLine(String expectedResourceName, String actualResourceName) {
        URL expectedResourceURL = Resources.getResource(expectedResourceName);
        URL actualResourceURL = Resources.getResource(actualResourceName);
        
        assertEqualsPerLine(expectedResourceURL, actualResourceURL);
    }

    public void assertEqualsPerLine(File expectedFile, File actualFile) {
        try {
            assertEqualsPerLine(expectedFile.toURI().toURL(), actualFile.toURI().toURL());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void assertEqualsPerLine(URL expectedURL, URL actualURL) {
        try {
            List<String> expectedLines = Resources.readLines(expectedURL, charset);
            List<String> actualLines = Resources.readLines(actualURL, charset);
            
            int commonLines = Math.min(expectedLines.size(), actualLines.size());
            
            List<String> comparisonFailures = new ArrayList<>();
            for (int line = 1; line <= commonLines; line++) {
                try {
                    int lineIndex = line-1;
                    Assert.assertEquals(expectedLines.get(lineIndex), actualLines.get(lineIndex));
                } catch (ComparisonFailure e) {
                    comparisonFailures.add(lineIdentifier(line) + e.getMessage());
                }
            }
            
            if (expectedLines.size() > commonLines) {
                for (int line = commonLines + 1; line <= expectedLines.size(); line++) {
                    int lineIndex = line-1;
                    comparisonFailures.add(lineIdentifier(line) + "expected:<" + expectedLines.get(lineIndex) + "> but was none");
                }    
            } else if (actualLines.size() > commonLines) {
                for (int line = commonLines + 1; line <= actualLines.size(); line++) {
                    int lineIndex = line-1;
                    comparisonFailures.add(lineIdentifier(line) + "expected none but was:<" + actualLines.get(lineIndex) + ">");
                }    
            }
            
            if (!comparisonFailures.isEmpty())
                throw new AssertionError(String.join("\n", comparisonFailures));
            
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String lineIdentifier(int line) {
        return "line " + line + ": ";
    }
}
