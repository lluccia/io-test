package dev.conca.fileiotest;

import static dev.conca.fileiotest.matchers.RegexMatcher.matchesRegex;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

import dev.conca.fileiotest.exception.ProcessExecutionException;
import dev.conca.fileiotest.exception.ProcessUndefinedException;

public class FileIOTest {

    public static final String LINE_SEPARATOR = System.getProperty("line.separator");

    private Map<String, String> environment = new HashMap<>();
    private List<String> command;
    
    private String consoleOutput;

    public void putEnvironment(String key, String value) {
        environment.put(key, value);
    }
    
    public void setProcess(String... command) {
        this.command = Arrays.asList(command);
    }

    public void execute() {
        if (command == null)
            throw new ProcessUndefinedException("Process under test must be defined");

        ProcessBuilder processBuilder = new ProcessBuilder(command);
        processBuilder.redirectErrorStream(true);
        Map<String, String> processEnv = processBuilder.environment();
        
        environment.forEach((key, value) -> processEnv.put(key, value));
        
        try {
            Process process = processBuilder.start();

            readConsoleOutput(process);
        } catch (IOException e) {
            throw new ProcessExecutionException(e);
        }
    }

    public void copyInputFileFromResource(String referenceInputResource, File destinationFile) {
        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(destinationFile))) {
            URL resourceUrl = Resources.getResource(referenceInputResource);
            Resources.copy(resourceUrl, outputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void readConsoleOutput(Process process) throws IOException {
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(process.getInputStream()));
        StringBuilder builder = new StringBuilder();
        String line = null;
        while ( (line = reader.readLine()) != null) {
           builder.append(line);
           builder.append(LINE_SEPARATOR);
        }
        this.consoleOutput = builder.toString();
    }

    public void assertConsoleOutputIs(String expected) {
        assertEquals(expected.trim(), consoleOutput.trim());
    }

    public void assertConsoleOutputContains(String string) {
        assertThat(consoleOutput, containsString(string));
    }

    public void assertConsoleOutputMatches(String regex) {
        assertThat(consoleOutput, matchesRegex(regex));
    }

    public void assertFileIsEqualToResource(File file, String referenceResource) {
        assertFileIsEqualToResource(file, referenceResource, Charsets.UTF_8);
    }
    
    public void assertFileIsEqualToResource(File file, String referenceResource, Charset charset) {
        try {
            FileComparator fileComparator = new FileComparator(charset);
            URL expectedFileResourceUrl = Resources.getResource(referenceResource);
            
            fileComparator.assertEquals(expectedFileResourceUrl, file.toURI().toURL());
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public void assertFileIsEqualToResourcePerLine(File file, String referenceResource) {
        assertFileIsEqualToResourcePerLine(file, referenceResource, Charsets.UTF_8);
    }
    
    public void assertFileIsEqualToResourcePerLine(File file, String referenceResource, Charset charset) {
        try {
            FileComparator fileComparator = new FileComparator(charset);
            URL expectedFileResourceUrl = Resources.getResource(referenceResource);
            
            fileComparator.assertEqualsPerLine(expectedFileResourceUrl, file.toURI().toURL());
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }


}
